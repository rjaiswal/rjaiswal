package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    @Test
    public void boxComplete() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 3, 2);

        // Draw lines around a box at (1, 1)
        grid.drawHorizontal(1, 1, 1);
        grid.drawHorizontal(1, 2, 1);
        grid.drawVertical(1, 1, 1);
        grid.drawVertical(2, 1, 1);

        assertTrue(grid.boxComplete(1, 1), "Box at (1, 1) should be complete");
    }

    @Test
    public void testBoxNotComplete() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 3, 2);

        // Draw only three lines around a box at (1, 1)
        grid.drawHorizontal(1, 1, 1);
        grid.drawHorizontal(1, 2, 1);
        grid.drawVertical(1, 1, 1);

        assertFalse(grid.boxComplete(1, 1), "Box at (1, 1) should not be complete");
    }

    @Test
    public void drawHorizontal() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 3, 2);

        grid.drawHorizontal(1, 1, 1);

        Exception exception = assertThrows(IllegalStateException.class, () -> {
            grid.drawHorizontal(1, 1, 1);
        });

        String expectedMessage = "Line at (1, 1) is already drawn";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void drawVertical() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 3, 2);

        grid.drawVertical(1, 1, 1);

        Exception exception = assertThrows(IllegalStateException.class, () -> {
            grid.drawVertical(1, 1, 1);
        });

        String expectedMessage = "Line at (1, 1) is already drawn";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }
}